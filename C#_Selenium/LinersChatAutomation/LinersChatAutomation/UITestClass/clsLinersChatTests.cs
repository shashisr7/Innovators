﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LinersChatAutomation.PageObjects;

namespace LinersChatAutomation
{
    [TestFixture]
    public class clsLinersChatTests
    {
        IWebDriver _webDriver;

        [SetUp]
        public void init()
        {
            _webDriver = new ChromeDriver();
            Console.WriteLine("Browser loaded successfully.");
            _webDriver.Navigate().GoToUrl("https://apply.uq.edu.au/login");
            Console.WriteLine("Chat login page loaded successfully.");
        }
        [Test]
        //[DataSource("Microsoft.VisualStudio.TestTools.UnitTest.CSV", "|datadirectory|Datasetup/LoginTest.CSV", "", DataAccessMethod.Sequential)]
        public void LoginTest()
        {
            clsLoginClass lc = new clsLoginClass(_webDriver);
            lc.LoginDetails();
        }
        [TearDown]
        public void cleanUp()
        {
            _webDriver.Close();
            Console.WriteLine("Login test completed successfully.");
        }
    }
}
