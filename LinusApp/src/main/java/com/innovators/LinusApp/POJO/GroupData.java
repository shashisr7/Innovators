package com.innovators.LinusApp.POJO;

import java.io.Serializable;
import java.util.List;

import com.innovators.LinusApp.Entities.Group;

public class GroupData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Group> groupData;

	public List<Group> getGroupData() {
		return groupData;
	}

	public void setGroupData(List<Group> groupData) {
		this.groupData = groupData;
	}
	
	

}
