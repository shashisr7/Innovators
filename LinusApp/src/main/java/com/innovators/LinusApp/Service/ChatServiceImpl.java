package com.innovators.LinusApp.Service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.innovators.LinusApp.POJO.ChatDetail;
import com.innovators.LinusApp.POJO.ChatGroup;
import com.innovators.LinusApp.Repository.ChatDao;

@Service
public class ChatServiceImpl implements ChatService{
	
	@Autowired
	ChatDao chatDao;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ChatServiceImpl.class);
	
	/* This method id invoked to get all the list of chat users */
	public List<ChatGroup> getGroupList(){
		LOGGER.info("::::::: Inside the method getGroupList method of ChatServiceImpl ::::");
		return chatDao.getGroupList();
	}
	
	/* This method is invoked to get the chatdetail of a particular group */
	public ChatDetail getChatDetailById(int id){
		LOGGER.info("::::::: Inside the method getChatDetailById method of ChatServiceImpl ::::");
		return chatDao.getChatDetailById(id);
	}

}
