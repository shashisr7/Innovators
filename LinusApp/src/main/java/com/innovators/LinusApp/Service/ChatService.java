package com.innovators.LinusApp.Service;

import java.util.List;

import com.innovators.LinusApp.POJO.ChatDetail;
import com.innovators.LinusApp.POJO.ChatGroup;

public interface ChatService {

	public List<ChatGroup> getGroupList();
	public ChatDetail getChatDetailById(int id);
}
