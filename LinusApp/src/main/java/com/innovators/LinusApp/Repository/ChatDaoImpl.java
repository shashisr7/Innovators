package com.innovators.LinusApp.Repository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.innovators.LinusApp.POJO.ChatDetail;
import com.innovators.LinusApp.POJO.ChatGroup;

@Repository
public class ChatDaoImpl implements ChatDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChatDaoImpl.class);

	@Autowired
	public JdbcTemplate template;

	/* This method id invoked to get all the list of chat users */
	public List<ChatGroup> getGroupList() {

		LOGGER.info("::::::: Inside the method getGroupList method of ChatDaoImpl ::::");

		String query = "select *  from tb_group";

		return template.query(query, new ChatGroupRowMapper());

	}

	/* This method is invoked to get the chatdetail of a particular group */
	@Override
	public ChatDetail getChatDetailById(int id) {

		LOGGER.info("::::::: Inside the method getChatDetailById method of ChatDaoImpl ::::");

		String sqlQuery = "SELECT message,name,time FROM tb_chat where gid= ? ";

		return template.queryForObject(sqlQuery, new Object[] {id}, new ChatDetailRowMapper() );

	}

}


