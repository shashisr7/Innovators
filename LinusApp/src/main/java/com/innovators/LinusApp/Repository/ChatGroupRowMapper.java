package com.innovators.LinusApp.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.innovators.LinusApp.POJO.ChatGroup;

public class ChatGroupRowMapper implements RowMapper<ChatGroup> {
	
	@Override
	public ChatGroup mapRow(ResultSet resultSet, int arg1) throws SQLException {

		ChatGroup chatGroup = new ChatGroup();
		chatGroup.setGroupId(resultSet.getInt("gid"));
		chatGroup.setGroupName(resultSet.getString("gname"));
		return chatGroup;
		
	}

}
