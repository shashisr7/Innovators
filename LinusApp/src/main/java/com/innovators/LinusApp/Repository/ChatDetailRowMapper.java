package com.innovators.LinusApp.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.innovators.LinusApp.POJO.ChatDetail;

public class ChatDetailRowMapper implements RowMapper<ChatDetail> {
	
	@Override
	public ChatDetail mapRow(ResultSet resultSet, int arg1) throws SQLException {

		ChatDetail chatDetail = new ChatDetail();
		chatDetail.setChatMessage(resultSet.getString("message"));
		chatDetail.setGroupName(resultSet.getString("name"));
		chatDetail.setChatTime(resultSet.getString("time"));
		return chatDetail;
		
	}

}
