package com.innovators.LinusApp.Repository;

import java.util.List;

import com.innovators.LinusApp.POJO.ChatDetail;
import com.innovators.LinusApp.POJO.ChatGroup;

public interface ChatDao {

	public List<ChatGroup> getGroupList();
	public ChatDetail getChatDetailById(int id);
}
