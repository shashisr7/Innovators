package com.innovators.LinusApp.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.innovators.LinusApp.Entities.Group;
import com.innovators.LinusApp.POJO.GroupData;
import com.innovators.LinusApp.Service.LinusAppService;

@RestController
@RequestMapping(value="/linus")
public class LinusAppController {
	
	@Autowired
	private LinusAppService linusAppService;

	@RequestMapping(value="/createGroup", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public void createGroup(@RequestBody Group group){
		linusAppService.createGroup(group);
	}
	
	@RequestMapping(value="/getAllGroups", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public GroupData getAllGroups(){
		return linusAppService.getAllGroups();
	}
	
	@RequestMapping(value="/getGroup/{groupId}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public GroupData getGroupById(@PathVariable("groupId") Integer groupId){
		return linusAppService.getGroupById(groupId);
	}
}
