package com.innovators.LinusApp.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="chat")
public class Chat {
	
	@Column(name="cid")
	private Integer cid;
	
	@Column(name="gid")
	private Integer gid;
	
	@Column(name="uname")
	private String uname;
	
	@Column(name="message")
	private String message;
	
	@Column(name="timestamp")
	private String timestamp;

	public Integer getCid() {
		return cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	public Integer getGid() {
		return gid;
	}

	public void setGid(Integer gid) {
		this.gid = gid;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public Chat() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Chat(Integer cid, Integer gid, String uname, String message, String timestamp) {
		super();
		this.cid = cid;
		this.gid = gid;
		this.uname = uname;
		this.message = message;
		this.timestamp = timestamp;
	}
	
	
	

}
