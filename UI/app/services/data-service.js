(function(){
  angular.module('myApp')
  .service('dataService',dataService);

  dataService.$inject = ['$q'];

  function dataService($q){
    var self = this;
    self.state = {
      user    : ''
    };
  
    self.setData = function(key, value){
      // var deferred = $q.defer;
      self.state[key] = value;
      // deferred.resolve('success');
      // return deferred.promise;
    };

    self.getData = function(key){
      return self.state[key];
    };
  }
})();
