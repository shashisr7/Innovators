(function(){
  'use strict'

  angular.module('myApp.login')
  .component('loginComponent', {
    templateUrl: 'components/login/login-template.html',
    controller: ('loginController', loginController)
  });
  
  loginController.$inject = ['$state','dataService'];
  
  function loginController($state,dataService) {
    var self = this;

    self.state = {
      user    : ''
    };

    self.submit = function(){
      console.log('username entered ' + self.state.user);
      dataService.setData('user',self.state.user)
      $state.go('chat');
    }
  }
})();

