(function(){
  'use strict'

  angular.module('myApp.chat')
  .component('chatComponent', {
    templateUrl: 'components/chat/chat-template.html',
    controller: ('chatController', chatController)
  });
  
  chatController.$inject = ['dataService','dataFactory'];
  
  function chatController(dataService, dataFactory) {
    var self = this;

    self.state = {
      user        : dataService.getData('user'),
      groupId     : '',
      groupNames          : [],
      selectedGroupIndex  : 0,
      chats               : []
    };

    self.getChats = function(){
      // console.log("Chats");
      dataFactory.getChats(self.selectedGroupIndex).then(function(response){
        // console.log('control returned to controller');
        console.log(response);
        self.state.chats = response.data.chats;
      });
    };

    (function(){
      dataFactory.getGroups().then(function(response){
        // console.log('control returned to controller');
        console.log(response);
        self.state.groupNames = response.data.groupData;
      });

      self.getChats();

    })();
    
    self.submit = function(){
      console.log('username entered ' + self.state.user);
    };

    self.selectGroup = function(index){
      console.log('select group called index is ');
      console.log(index);
    };

    self.isGroupSelected = function(index){
      return index == self.state.selectedGroupIndex;
    };

    self.isChatFromSender = function(user){
      return user.toLowerCase() == self.state.user.toLowerCase(); 
    };

    console.log(self.state.user);
  }
})();

