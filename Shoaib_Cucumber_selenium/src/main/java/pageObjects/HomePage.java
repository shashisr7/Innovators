package pageObjects;
 
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
 
public class HomePage {
	
	WebDriver driver;
	
	@FindBy(xpath="//a/span[@class='gb_db gbii']") 
	private WebElement image;
	
	@FindBy(linkText="Sign out") 
	private WebElement btn_signout;
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public String gettext_Signout() {
		return util.gettext(btn_signout);

	}
	
	
	public void click_image() {
		image.click();
	}
	

	public void click_Signout() {
		btn_signout.click();
	}
 
}