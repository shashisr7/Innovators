package pageObjects;
 
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
 
public class PasswordPage {
	
	WebDriver driver;
	
	@FindBy(xpath="//content//span[contains(text(),'Next')]") 
	private WebElement btn_next;
	
	@FindBy(name="password") 
	private WebElement passwrd;
	
	public PasswordPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	
	public void enter_password(String pwd) {
		passwrd.sendKeys(pwd);
	}
	

	public void click_Nextbtn() {
		btn_next.click();
	}
 
}