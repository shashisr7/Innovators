package stepdefinitions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.cucumber.listener.Reporter;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import managers.PageObjectManager;
import pageObjects.GmailSignInPage;
import pageObjects.HomePage;
import pageObjects.PasswordPage;

public class stepdef {

	public static WebDriver driver;
	GmailSignInPage gpage;
	PasswordPage ppage;
	HomePage hpage;
	PageObjectManager pageObjectManager;

	
	@Given("^User is on Home Page$")
	public void user_is_on_Home_Page() throws Throwable {
				
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Shoaib\\Desktop\\chromedriver_win32\\chromedriver.exe");
		//System.setProperty("webdriver.gecko.driver", "C:\\Users\\Shoaib\\Downloads\\geckodriver-v0.21.0-win64\\geckodriver.exe");
		
		
		/*
		DesiredCapabilities capabilities = new DesiredCapabilities();

		 capabilities = DesiredCapabilities.firefox();
		 capabilities.setBrowserName("firefox");
		 capabilities.setVersion("46.0");
		 capabilities.setPlatform(Platform.WINDOWS);
		 capabilities.setCapability("marionette", false);*/
		
		
		
		//driver = new FirefoxDriver(capabilities);

		 driver=new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://accounts.google.com/signin/v2/identifier?flowName=GlifWebSignIn&flowEntry=ServiceLogin");
        driver.manage().window().maximize();
		Reporter.addStepLog("Google Chrome Browser launched");


		}
	
	
	
	@When("^User Navigate to LogIn Page$")
	public void user_Navigate_to_LogIn_Page() throws Throwable {
		pageObjectManager=new PageObjectManager(driver);

		gpage=pageObjectManager.getGmailSignInPage();
		gpage.enter_emailID("shoaib.shahulhameed");
		gpage.click_btn_Next();
				
        Reporter.addStepLog("User navigated to Login Page");

		}
	
	@And("^User enters \"(.*)\" and \"(.*)\"$")
	public void user_enters_UserName_and_Password(String username, String password) throws Throwable {
		ppage=pageObjectManager.getPasswordPage();
		ppage.enter_password("ammar2015");
		
		 WebDriverWait wait = new WebDriverWait(driver,20);
		    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//content//span[contains(text(),'Next')]")));
		    
		ppage.click_Nextbtn();
		
		Reporter.addStepLog("User entered credentials");

	
	}
	
	@Given("^User enters \"(.*)\" and \"(.*)\" incorrectly$")
	public void user_enters_UserName(String username, String password) throws Throwable {
		
		try
		{
			
			System.setProperty("webdriver.chrome.driver","C:\\Users\\Shoaib\\Desktop\\chromedriver_win32\\chromedriver.exe");
			//System.setProperty("webdriver.gecko.driver", "C:\\Users\\Shoaib\\Downloads\\geckodriver-v0.21.0-win64\\geckodriver.exe");
			
			
			/*
			DesiredCapabilities capabilities = new DesiredCapabilities();

			 capabilities = DesiredCapabilities.firefox();
			 capabilities.setBrowserName("firefox");
			 capabilities.setVersion("46.0");
			 capabilities.setPlatform(Platform.WINDOWS);
			 capabilities.setCapability("marionette", false);*/
			
			
			
			//driver = new FirefoxDriver(capabilities);

			 driver=new ChromeDriver();
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	        System.out.println("Validate downloads page header text");
			
			driver.get("https://shop.polymer-project.org/");
	        WebElement root1 = driver.findElement(By.xpath("//shop-app[@page='home']"));

	                //Get shadow root element
			WebElement shadowRoot1 = expandRootElement(root1);

			WebElement root2 = shadowRoot1.findElement(By.cssSelector("app-header div shop-tabs shop-tab:nth-child(2) a"));

			String divID = root2.getText();
			System.out.println("divID text:"+ divID);
			
			root2.click();
			
			// Verify header title
	/*		Assert.assertEquals("Downloads", actualHeading);
	*/		
			
			
	        Reporter.addStepLog("Google Chrome Browser launched");
		/*driver.findElement(By.name("pasrd")).sendKeys("ammar2015");
		driver.findElement(By.xpath("//span[contains(text(), 'Next')]")).click();
*/
		}
		catch(Exception e)
		{
			System.out.println("getmessage" + e.getMessage());
			driver.close();
	

		}
        Reporter.addStepLog("User entered credentials");

	
	}
	
	@Then("^Message displayed Login Successfully$")
	public void message_displayed_Login_Successfully() throws Throwable {
		
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		hpage=pageObjectManager.getHomePage();
		hpage.click_image();
		String btntxt=hpage.gettext_Signout();
		
		if(btntxt.equals("Sign out"))
		{
	        Reporter.addStepLog("User logged in successfully");

		}
		
		
	}

	@When("^User LogOut from the Application$")
	public void user_LogOut_from_the_Application() throws Throwable {

		hpage.click_Signout();


	}
	

	@Then("^Message displayed Logout Successfully$")
	public void message_displayed_Logout_Successfully() throws Throwable {
		
		boolean frgtpwddisplay=driver.findElement(By.xpath("//h1[text()='Choose an account']")).isDisplayed();
		if(frgtpwddisplay)
			System.out.println("LogOut Successfully");
		
	
		}
	
	@Then("^Close the browser$")
	public void close_browser() throws Throwable {
		
		driver.close();
	}
	
	public WebElement expandRootElement(WebElement element) {
		WebElement ele = (WebElement) ((JavascriptExecutor) driver).executeScript("return arguments[0].shadowRoot",element);
		return ele;
	}
		
	
}
