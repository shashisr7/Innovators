package managers;
 
 
 
import org.openqa.selenium.WebDriver;
 
import pageObjects.GmailSignInPage;
 
import pageObjects.HomePage;
 
import pageObjects.PasswordPage;
import pageObjects.util;
 
public class PageObjectManager {
 
	private WebDriver driver;
 
	private GmailSignInPage gpage;
 
	private HomePage hpage;
 
	private PasswordPage ppage;
	
	private util utlobj;
 

	public PageObjectManager(WebDriver driver) {
 
		this.driver = driver;
 
	}
 
	
 
	public GmailSignInPage getGmailSignInPage(){
 
		return (gpage == null) ? gpage = new GmailSignInPage(driver) : gpage;
 
	}
 
	
 
	public HomePage getHomePage() {
 
		return (hpage == null) ? hpage = new HomePage(driver) : hpage;
 
	}
 
	
 
	public PasswordPage getPasswordPage() {
 
		return (ppage == null) ? ppage = new PasswordPage(driver) : ppage;
 
	}
 
	public util getutilobj() {
		 
		return (utlobj == null) ? utlobj = new util(driver) : utlobj;
 
	}
	
 
	
}